const { merge } = require("webpack-merge");
const defaultConfigFactory = require("./config/default.webpack.config");

module.exports = () =>
  merge(defaultConfigFactory(process.env.NODE_ENV), {
    // Custom config
  });
