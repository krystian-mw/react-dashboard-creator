"use strict";

process.env.BABEL_ENV = "development";
process.env.NODE_ENV = "development";

process.on("unhandledRejection", (err) => {
  throw err;
});

require("../config/env");

const fs = require("fs");
const webpack = require("webpack");
const clearConsole = require("react-dev-utils/clearConsole");
const chalk = require("react-dev-utils/chalk").default;
const checkRequiredFiles = require("react-dev-utils/checkRequiredFiles");
const {
  createCompiler,
  prepareUrls,
} = require("react-dev-utils/WebpackDevServerUtils");
const openBrowser = require("react-dev-utils/openBrowser");
const paths = require("../config/paths");
const configFactory = require("../webpack.config");

const useYarn = fs.existsSync(paths.yarnLockFile);
const isInteractive = process.stdout.isTTY;

if (!checkRequiredFiles([paths.appHtml, paths.appIndexJs])) {
  process.exit(1);
}

const PORT = parseInt(process.env.PORT, 10) || 3000;
const HOST = process.env.HOST || "0.0.0.0";

const { checkBrowsers } = require("react-dev-utils/browsersHelper");
checkBrowsers(paths.appPath, isInteractive)
  .then(() => {
    const config = configFactory("development");
    const protocol = process.env.HTTPS === "true" ? "https" : "http";
    const appName = require(paths.appPackageJson).name;
    const useTypeScript = fs.existsSync(paths.appTsConfig);
    const tscCompileOnError = process.env.TSC_COMPILE_ON_ERROR === "true";
  

    const compiler = webpack(config)

    const watching = compiler.watch({}, (err, stats) => {
    //   clearConsole();
      console.log(chalk.cyanBright("Client:"));
      console.log(err || chalk.greenBright('Success! Stats coming soon ...'));
    });

    require('../app').listen(PORT, HOST, (err) => {
        // clearConsole();
        console.log(chalk.cyanBright("Server:"));
        console.log(err || chalk.greenBright('Success! Stats coming soon ...'));
    })

    if (isInteractive || process.env.CI !== "true") {
      process.stdin.on("end", function () {
        watching.close();
        process.exit();
      });
      process.stdin.resume();
    }
  })
  .catch((err) => {
    if (err && err.message) {
      console.log(err.message);
    }
    process.exit(1);
  });
